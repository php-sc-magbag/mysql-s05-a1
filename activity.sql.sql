SELECT customerName FROM customers WHERE country ="Philippines";
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
SELECT customerName FROM customers WHERE state is NULL;

SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" && lastName = "Patterson";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country IN ("USA", "FRANCE", "Canada");

SELECT employees.firstName, employees.lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

SELECT productName, customerName  FROM customers 
JOIN orders ON customers.customerNumber = orders.customerNumber

JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber

JOIN products ON  orderdetails.productCode = products.productCode

WHERE customers.customerName LIKE "%Baane Mini Imports%";

SELECT DISTINCT employees.firstName, employees.lastName, customerName, offices.country FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE  customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" && quantityInStock > 1000;

SELECT customerName FROM customers WHERE phone LIKE "%+81%";